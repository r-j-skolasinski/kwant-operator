from math import inf
import numpy as np
import itertools as it
import functools as ft
import operator
import abc
import weakref


class SiteArray:
    __slots__ = ('space', 'tags')

    def __init__(self, space, tags):
        self.space = space
        self.tags = tags = np.array(tags, int)
        if tags.ndim != 2:
            raise ValueError("Expecting a 2-dimensional array-like for 'tags'.")
        sfs = space.finite_shape
        snid = space.num_inf_dims
        if tags.shape[1] != snid + len(sfs):
            raise ValueError(f"Expecting tags of length {snid + len(sfs)}.")
        for col, size in zip(tags.T[snid:], sfs):
            if np.amin(col) < 0 or np.amax(col) >= size:
                raise ValueError("Tag components out of bounds.")

    def __repr__(self):
        return 'SiteArray({0},\n{1})'.format(repr(self.space), repr(self.tags))

    def __eq__(self, other):
        return (isinstance(other, SiteArray)
                and self.space == other.space
                and np.array_equal(self.tags, other.tags))

    def __len__(self):
        return len(self.tags)

    def __getitem__(self, spec):
        return SiteArray(self.space, np.atleast_2d(self.tags[spec]))


class HilbertSpace(metaclass=abc.ABCMeta):
    @abc.abstractproperty
    def components(self):
        pass

    @abc.abstractproperty
    def names(self):
        pass

    @abc.abstractproperty
    def num_inf_dims(self):
        pass

    @abc.abstractproperty
    def finite_shape(self):
        pass

    def __hash__(self):
        return hash(self.names)

    def __eq__(self, other):
        # The following works because Hilbert spaces with the same name are
        # enforced to be equal.
        return isinstance(other, HilbertSpace) and self.names == other.names

    def __mul__(self, other):
        if not isinstance(other, HilbertSpace):
            raise TypeError("Cannot multiply Hilbert space by "
                            f"'{type(other).__name__}' instance.")
        return Product(*self.components, *other.components)

    def has_subspace(self, other):
        return _is_subsequence(other.names, self.names)


class Simple(HilbertSpace):
    _registry = weakref.WeakValueDictionary()

    def __init__(self, size, name):
        if size == inf:
            self._size = inf
        else:
            self._size = int(size)
            if self._size != size:
                raise TypeError("The size of a simple Hilbert space must be"
                                "an integer or inf.")

        if not isinstance(name, str):
            raise TypeError(
                "The name of a simple Hilbert space must be a string.")
        self._name = name

        other = Simple._registry.setdefault(name, self)
        if other._size != size:
            raise ValueError("A different simple Hilbert space with the same"
                             " name already exists.")

    @property
    def components(self):
        return (self,)

    @property
    def names(self):
        return (self._name,)

    @property
    def num_inf_dims(self):
        return int(self._size == inf)

    @property
    def finite_shape(self):
        return () if self._size == inf else (self._size,)

    def __repr__(self):
        return f'{self.__class__.__name__}({self._size}, "{self._name}")'


class Product(HilbertSpace):
    """Direct Product of simple Hilbert spaces."""
    def __init__(self, *components):
        n = len(components)
        if n < 2:
            raise ValueError('Must provide at least 2 components for a '
                             'product space.')
        if n != len(set(components)):
            raise ValueError('Direct product may not contain multiple copies '
                             'of the same Hilbert space.')

        shape = []
        ninf = 0
        for c in components:
            if not isinstance(c, Simple):
                raise ValueError('Only products of simple Hilbert spaces are '
                                 'allowed.')
            if c._size != inf:
                shape.append(c._size)
            else:
                if shape:
                    raise ValueError('Infinite Hilbert spaces must come '
                                     'before finite ones in a product.')
                ninf += 1
        self._shape = tuple(shape)
        self._ninf = ninf
        self._components = components

    @property
    def components(self):
        return self._components

    @property
    def names(self):
        return tuple(c._name for c in self._components)

    @property
    def num_inf_dims(self):
        return self._ninf

    @property
    def finite_shape(self):
        return self._shape

    def __repr__(self):
        return "".join([self.__class__.__name__,
                        "(",
                        ", ".join(repr(c) for c in self._components),
                        ")"])


def _is_subsequence(x, y):
    it = iter(y)
    return all(c in it for c in x)
