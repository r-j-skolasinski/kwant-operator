import kwant

### 2D example

lat = kwant.lattice.square()

# Scattering region Hamiltonian
def onsite(site):
    return site.pos**2

H_0 = kwant.FiniteSupport(lat, shape, onsite)
P_s = kwant.Projector(H_0.sites)
hops = [kwant.Periodic(lat.sym, (lat(*delta), lat(0, 0)), -1)
        for delta in lat.prim_vecs)]
V = sum(hops)

H_s = H_0 + P_s @ (V + V.dagger()) @ P_s

# Lead hamiltonian
H_0l = Kwant.Periodic(lat.sym, lat(0, 0), value)
H_l = H_0l + V + V.dagger()

# System hamiltonian
H_tot = H_s + H_l - P_s @ H_l @ P_s

# What we want to calculate
R = kwant.Diagonal(lat.pos)
P_window = kwant.Projector(lambda site: all(abs(i) < 100 for i in site))
R_window = P_window @ R @ P_window

# Calculate stuff

solution = kwant.scatter(H_tot, energy=0,
                         bulk_algorithm=kwant.physics.modes_nd,
                         matching=kwant.physics.dyson,
                         )

fermi_surface = solution.modes()

solution.scattering_amplitude(in_angle, out_angle)

solution.expectation(R_window)

solution.greens_function(lat(10000, 0), lat(0, 0))


### 1D example
