from operator import itemgetter
from collections import defaultdict, OrderedDict, namedtuple

from kwoperator import *

# Helper function for adapters

def dimension(operator):
    """Return the number of translational symmetry directions for an operator"""
    if isinstance(operator, Algebra):
        raise NotImplementedError()
    if isinstance(operator, (FiniteSupport, Projector)):
        return 0
    elif isinstance(operator, (TranslationallyInvariant,
                               HalfTranslationallyInvariant)):
        return len(operator.sym.periods)


def min_dimension(term):
    return min(map(dimension, term))


def group_by_dimension(operator : Algebra):
    """Group the terms in an algebra by their translational symmetry dimension"""
    # Dimension of a product of operators is the minimum of
    # their individual dimensions
    sorted_operator = sorted(operator.operators, key=min_dimension)
    return [
        (dim, Algebra(list(ops)))
        for dim, ops in itertools.groupby(sorted_operator, key=min_dimension)
    ]


def direct_sum_finite_support(operators):
    """Take the direct sum of a list of finite support operators.

    Even identical sites are treated as distinct, meaning the 'sites'
    attribute of the output operator may contain duplicates.
    """
    assert all(isinstance(op, FiniteSupport)
               for op in operators)
    sites, cols, rows, vals = [], [], [], []
    for op in operators:
        r, c, v = op.matrix
        rows.append(r + len(sites))
        cols.append(c + len(sites))
        vals.append(v)
        sites.extend(op.sites)

    matrix = tuple(map(np.hstack, (rows, cols, vals)))
    return FiniteSupport(matrix, sites)


def compress(sequence):
    # Construct map from index within 'sequence'
    # to index within 'compressed'
    idx_map = {}
    first_seen = OrderedDict()
    for i, x in enumerate(sequence):
        if x not in first_seen:
            first_seen[x] = len(first_seen)
        idx_map[i] = first_seen[x]
    # Order unique elements by when they were first seen
    compressed, _ = zip(*sorted(first_seen.items(), key=itemgetter(1)))
    return compressed, idx_map


def sum_finite_support(operators):
    """Sum a list of finite support operators"""
    assert all(isinstance(op, FiniteSupport)
               for op in operators)
    # First take the direct sum of operators, treating
    # (even identical) sites as unique
    op = direct_sum_finite_support(operators)

    # Compress identical sites
    sites, idx_map = compress(op.sites)
    # Map row and column indices to use the compressed sites
    rows, cols, vals = op.matrix
    rows = np.array([idx_map[r] for r in rows])
    cols = np.array([idx_map[r] for r in cols])
    return FiniteSupport((rows, cols, vals), sites)


def sum_translationally_invariant(operators):
    """Sum a list of translationally invariant operators"""
    assert all(isinstance(op, TranslationallyInvariant)
               for op in operators)
    sym = operators[0].sym
    assert all(op.sym == sym for op in operators)
    h_f = sum_finite_support([op.h_f for op in operators])
    return TranslationallyInvariant(sym, h_f)


SymmetryDecomposed = namedtuple('SymmetryDecomposed',
                                ['sym', 'sites', 'components'])

ImpuritySpecification = namedtuple('ImpuritySpecification',
                                   ['hamiltonian', 'sites'])

def decompose_by_symmetry(bulk : Algebra):
    # Decompose a sum of translationally invariant operators into
    # components that connect different unit cells.
    assert all(len(factors) == 1 for factors in bulk.operators)
    op = sum_translationally_invariant([op for (op,) in bulk.operators])
    sites, sym = op.h_f.sites, op.sym
    by_symmetry = defaultdict(list)
    for (a, b, v) in zip(*op.h_f.matrix):
        sa, sb = sites[a], sites[b]
        t = sym.which(sb) - sym.which(sa)
        by_symmetry[t].append((a, b, v))

    # Make each component a bona-fide triple of numpy arrays
    components = []
    for t, matrix in by_symmetry.items():
        matrix = tuple(map(np.hstack, zip(*matrix)))
        components.append((t, matrix))

    return SymmetryDecomposed(sym, sites, components)

# An adapter

def greens_function(alg : Algebra):

    dimensions, operators = zip(*group_by_dimension(alg))

    if dimensions == (2,):
        bulk, = operators
        return decompose_by_symmetry(bulk)
    elif dimensions == (0, 2):
        impurity, bulk = operators
        bulk_part = decompose_by_symmetry(bulk)
        impurity_part = sum_finite_support([op for (op,) in impurity.operators])
        impurity_part = ImpuritySpecification(impurity_part.matrix,
                                              impurity_part.sites)
        return (bulk_part, impurity_part)
    else:
        raise NotImplementedError()
