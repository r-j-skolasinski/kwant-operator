{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Kwant 2 formats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hilbert spaces"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "import hilbert_space"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are 2 kinds of \"simple\" Hilbert space that we will consider:\n",
    "\n",
    "+ Infinite dimensional, with `d` directions. An element is labelled\n",
    "  by an element of `ℤ^d`.\n",
    "+ Finite (`n`) dimensional (e.g. spin 1/2). An element is labelled by\n",
    "  an integer `0 <= i < n`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Could be a 2D lattice (but we haven't specified what yet!)\n",
    "lat = hilbert_space.Infinite(ndim=2, name='square')\n",
    "# Could be a Landau level\n",
    "landau = hilbert_space.Infinite(ndim=1, name='landau')\n",
    "# Could be a spin\n",
    "spin = hilbert_space.Finite(norbs=2, name='spin')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may construct \"composite\" Hilbert spaces that are direct products and direct sums of these basic types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H = lat * spin + landau"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because we wish to uniquely label all elements of the Hilbert space, a given simple Hilbert space may only appear once in a given composite Hilbert space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    lat + lat\n",
    "except ValueError as e:\n",
    "    print(e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also determine whether one hilbert space is a subspace of another:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H.has_subspace(lat), H.has_subspace(lat * spin)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spin.has_subspace(lat), spin.has_subspace(H)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sites"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we \"call\" a Hilbert space with integers we get a `Site`. One should interpret a `Site` as a *subspace of a Hilbert space*. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lat(0, 1) in lat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`lat(0, 1)` is a 1 dimensional subspace of `lat` (which we may identify with a ray),"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "landau(0) in H"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`landau(0) in H` is a 1 dimensional subspace of `lat * spin + landau`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(lat * spin)(0, 0, 0) in H"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may *not* create sites directly in the direct sum space `H`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    H(0, 1, 2)\n",
    "except NotImplementedError:\n",
    "    print('NotImplementedError')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A more subtle aspect is how to interpret this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lat(0, 0) in H"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`lat(0, 0)` is a 2 dimensional subspace of `H`, which is spanned by `(lat * spin)(0, 0, 0), (lat * spin)(0, 0, 1)`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may multiply sites together to get sites in the product space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lat(0, 0) * spin(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Site Arrays"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Often we will want to store sites that belong to the same Hilbert space together.\n",
    "\n",
    "It is wasteful to store the tag and space separately every time, so we may define arrays of sites that belong to the same Hilbert space: `SiteArray`s.\n",
    "\n",
    "We may create these using the `array` method of Hilbert spaces"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rectangle = lat.array(np.mgrid[0:500, 0:1000].transpose().reshape(-1, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may multiply site arrays with sites to get site arrays in the product space"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rectangle * spin(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Operators"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import kwoperator as op"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Explicit operators"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This class of operators is an explicit collection of matrix elements.\n",
    "\n",
    "We may build such an operator by providing 2 `SiteArray`s and a sequence of values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const = op.Explicit.from_list(rectangle, rectangle, np.arange(len(rectangle)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may also provide a callable that takes 2 site arrays (and other parameters) as argument), and returns the sequence of values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(to_sites, from_sites, V):\n",
    "    return V * np.ones(len(to_sites))\n",
    "\n",
    "computed = op.Explicit.from_list(rectangle, rectangle, f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then call the operator to get a COO matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "computed(V=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When defining an operator over a product space (e.g. `lat * spin`) we may wish to split the space into \"base space\" (e.g. `lat`) and \"fiber space\" (e.g. `spin`) and express the operator in a basis of the base space with values being explicit matrices in the fiber space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values = np.outer(np.arange(len(rectangle)), np.eye(2)).reshape(500000, 2, 2)\n",
    "\n",
    "const = op.Explicit.from_list(rectangle, rectangle, values, value_space=(spin, spin))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also define operators directly in the product space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spin_hops = op.Explicit.from_list(rectangle * spin(1), rectangle * spin(0), np.ones(len(rectangle)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "spin_hops()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Projectors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `Projector` class represents a projector onto a subspace defined by a `SiteArray`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "P = op.Projector(lat.array([\n",
    "    [0, 0], [0, 1], [0, 2]\n",
    "]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may apply projectors to other operators:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "op.project(const, P)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Operator groups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may declare a *group* of operators with `TranslationGroup`. We may only create translation groups over infinite Hilbert spaces, and by definition the operators act in a way that is equivalent to a translation of site tags."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = op.TranslationGroup(lat, [[1, 0], [0, 1]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If there is an affine transformation between tags in `lat` and realspace then `T` is a *representation of a translational space group*.\n",
    "\n",
    "It is, however, important to remember that `TranslationGroup` really is defined as a group of *operators on a Hilbert space*.\n",
    "\n",
    "We can create operators equivalent to a given tag translation by *calling* the group:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tx = T(1, 0)\n",
    "ty = T(0, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Symmetrized operators"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have defined a group of operators we may *symmetrize* another operator with respect to this group:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "to_sites = lat.array([\n",
    "    [0, 0],\n",
    "    [1, 0],\n",
    "    [0, 1],\n",
    "])\n",
    "\n",
    "from_sites = lat.array([\n",
    "    [0, 0],\n",
    "    [0, 0],\n",
    "    [0, 0],\n",
    "])\n",
    "\n",
    "values = [\n",
    "    4 * np.eye(2),\n",
    "    -1 * np.array([[1, 0], [0, -1]]),\n",
    "    -1 * np.array([[1, 0], [0, -1]]),\n",
    "]\n",
    "\n",
    "\n",
    "H_f = op.Explicit.from_list(to_sites, from_sites, values, value_space=(spin, spin))\n",
    "H_p = H_f.symmetrized(T)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H_p"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may also use projectors with these symmetrized operators:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scattering_region = op.project(H_p, P, include_hoppings=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may also *symmetrize the projectors* and use them as before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Tx = T.subgroup([[1, 0]])\n",
    "\n",
    "P_wire = P.symmetrized(Tx)\n",
    "\n",
    "wire = op.project(H_p, P_wire)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Adapters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "..."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Py3k",
   "language": "python",
   "name": "py3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
