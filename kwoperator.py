import abc
import itertools
import numpy as np
from operator import attrgetter
import scipy.sparse as sp

import hilbert_space
import symmetry

def _components(a):
    assert isinstance(a, Operator)
    return a.operators if isinstance(a, Algebra) else [[a]]


class Operator(metaclass=abc.ABCMeta):

    def __add__(self, other):
        if isinstance(other, Operator):
            return Algebra(_components(self) + _components(other))
        return NotImplemented

    def __matmul__(self, other):
        if isinstance(other, Operator):
            terms = itertools.product(_components(self), _components(other))
            return Algebra([a + b for a, b in terms])
        return NotImplemented

    @abc.abstractproperty
    def row_space(self):
        ...

    @abc.abstractproperty
    def column_space(self):
        ...

    @abc.abstractproperty
    def symmetry(self):
        ...


class Algebra(Operator):
    """Sequence of sequences of operators.
    '[[A, B], [C, B]]' means 'A @ B + C @ D'.
    """
    def __init__(self, operators):
        self.operators = operators
        super().__init__()


class OperatorGroup(metaclass=abc.ABCMeta):
    """A group of operators acting on a Hilbert space."""

    def has_subgroup(self, other):
        ...


class TrivialGroup(OperatorGroup):

    def has_subgroup(self, other):
        return isinstance(other, TrivialGroup)


class TranslationGroup(OperatorGroup):
    """A group of operators that translates tags.

    Operators in this group are infinite permutation matrices, but can be
    better understood by their action (translation) on tags.
    """

    def __init__(self, space, generators):

        space = hilbert_space.infinite_part(space)

        # Check generators
        generators = np.array(generators, int)
        if len(generators.shape) != 2:
            raise ValueError('Generators must be a 2D array')
        if generators.shape[1] != space.tag_length(space):
            raise ValueError('Generator dimension must match Hilbert space')
        if np.linalg.matrix_rank(generators) < len(generators):
            raise ValueError('Generators must be linearly independent')
        self.space = space
        self.generators = generators

    def __call__(self, *components):
        return Translation(self, np.array(components, int))

    def subgroup(self, generators):
        generators = np.array(generators)
        if generators.dtype != int:
            raise ValueError('Generators must be sequences of integers.')
        return TranslationGroup(self.space, np.dot(generators, self.generators))

    def has_subgroup(self, other):
        if isinstance(other, TrivialGroup):
            return True
        elif not (isinstance(other, TranslationGroup) and
                  self.space.has_subspace(other.space)):
            return False

        if other.generators.shape[1] != self.generators.shape[1]:
            return False  # Mismatch of spatial dimensionalities.

        inv = np.linalg.pinv(self.generators)
        factors = np.dot(other.generators, inv)
        # Absolute tolerance is correct in the following since we want an error
        # relative to the closest integer.
        return (np.allclose(factors, np.round(factors), rtol=0, atol=1e-8) and
                np.allclose(np.dot(factors, self.generators), other.generators))


class Translation(Operator):
    """A single operator from a TranslationGroup.

    Parameters
    ----------
    group : TranslationGroup
    vector : array of int
        Translation vector in the basis of 'group.generators'.
    """
    def __init__(self, group, vector):
        self.group = group
        self.vector= vector

    def __pow__(self, n):
        if int(n) != n:
            raise TypeError('Only integer powers are valid for translations')
        return Translation(self.group, int(n) * self.vector)

    @property
    def row_space(self):
        return self.group.space

    @property
    def column_space(self):
        return self.group.space

    @property
    def symmetry(self):
        return self.group


def _normalize_values(value_spaces, values):
    """Normalize operator matrix elements with respect to datatype and shape."""
    values = np.array(values, complex).squeeze()
    if (not all(value_spaces)) and len(values.shape) > 1:
        raise ValueError('Values are matrices, but '
                         'no value space specified.')
    # cast 'values' to canonical shape.
    dims = [s.dims if s is not None else 1 for s in value_spaces]
    axes = [slice(None)] + [slice(None) if dim > 1 else np.newaxis
                            for dim in dims]
    values = values[axes]
    if values.shape != (len(values), *dims):
        raise ValueError('Value array has incompatible shape '
                         'with the specified value spaces.')
    return values


def _total_space(base_space, value_space):
    if value_space is None:
        return base_space
    else:
        return base_space * value_space


def _index_tags(tags):
    """Return a sequence of unique tags and the indices of the original tags."""
    # Remove duplicates and calculate indices into the original tags array.
    # TODO: sort the tags into a canonical order?
    return np.unique(tags, axis=0, return_inverse=True)


def _expand_orbs(row_norbs, col_norbs, idxs, rows=True):
    if rows:
        expand = np.repeat
        a_norbs, b_norbs = row_norbs, col_norbs
    else:
        expand = np.tile
        a_norbs, b_norbs = col_norbs, row_norbs

    orb_offsets = expand(np.arange(a_norbs), b_norbs)
    orbs = (idxs * a_norbs).reshape(-1, 1) + orb_offsets.reshape(1, -1)
    return orbs.reshape(-1)


def _check_sym_compatibility(sym : OperatorGroup, operator : Operator):
    if isinstance(sym, TrivialGroup):
        return
    if not (operator.row_space.has_subspace(sym.space) and
            operator.column_space.has_subspace(sym.space)):
        raise ValueError('Symmetry group and operator are defined over '
                         'incompatible Hilbert spaces.')


class Explicit(Operator):
    """An operator with (a finite number of) explicitly specified matrix elements.

    Attributes
    ----------
    basis: (SiteArray, SiteArray)
        The basis for the column and row spaces.
    value_space: (HilbertSpace or None, HilbertSpace or None)
        Each Hilbert space must have finite dimension.
        The basis for in which the values are specified is assumed
        to be in the same order as the tags. If not specified then
        the values must be scalars.
    rows, cols : ndarray of int
        Indexes into basis[0] and basis[1] respectively.
    values : ndarray or callable
        Either an array with first axis the same length as rows/cols,
        or a function that returns such an array. If 'value_space'
        is not specified then the array must be 1 dimensional, otherwise
        it must have shape '(N, value_space[0].dims, value_space[1].dims)'.
    sym : OperatorGroup
        The group of operators with respect to which to symmetrize this operator.
    """

    def __init__(self, basis, value_space, rows, cols, values, sym=TrivialGroup()):

        if not callable(values):
            values = _normalize_values(value_space, values)

        self.basis = tuple(basis)
        self.value_space = tuple(value_space)
        self.rows, self.cols = rows, cols
        self.values = values

        _check_sym_compatibility(sym, self)
        self.sym = sym

    def __call__(self, *args, **kwargs):
        """Return the operator as a COO matrix."""
        if callable(self.values):
            # NOTE: This should be optimized. We effectively index a
            #       numpy array with another one, so this is not an efficient
            #       slicing operation.
            row_basis, col_basis = self.basis
            values = self.values(row_basis[self.rows], col_basis[self.cols],
                                 *args, **kwargs)
            values = _normalize_values(self.value_space, values)
        else:
            if args or kwargs:
                raise TypeError('This operator takes no parameters')
            values = self.values

        row_norbs = self.value_space[0].dims if self.value_space[0] else 1
        col_norbs = self.value_space[1].dims if self.value_space[1] else 1

        return sp.coo_matrix((
            values.reshape(-1),
            (_expand_orbs(row_norbs, col_norbs, self.rows, rows=True),
             _expand_orbs(row_norbs, col_norbs, self.cols, rows=False)),
        ))

    @property
    def row_space(self):
        return _total_space(self.basis[0].space, self.value_space[0])

    @property
    def column_space(self):
        return _total_space(self.basis[1].space, self.value_space[1])

    @property
    def symmetry(self):
        return self.sym

    def symmetrized(self, sym):
        if not isinstance(self.sym, TrivialGroup):
            raise TypeError('Only operators with trivial symmetry group '
                            'may be symmetrized.')
        return Explicit(self.basis, self.value_space,
                             self.rows, self.cols, self.values, sym)

    # Factories

    @staticmethod
    def from_list(to_sites, from_sites, values, value_space=(None, None)):
        # Remove duplicates and calculate row/column indices
        to_tags, rows = _index_tags(to_sites.tags)
        from_tags, cols = _index_tags(from_sites.tags)

        basis = (hilbert_space.SiteArray(to_sites.space, to_tags),
                 hilbert_space.SiteArray(from_sites.space, from_tags))

        return Explicit(basis, value_space, rows, cols, values)


class Projector(Operator):
    """A projector on a subspace spanned by a sequence of sites.

    Attributes
    ----------
    basis : SiteArray
    sym : OperatorGroup
        Operator with respect to which to symmetrize this projector.
    """

    def __init__(self, basis, sym=TrivialGroup()):
        self.basis = basis
        _check_sym_compatibility(sym, self)
        self.sym = sym

    @property
    def row_space(self):
        return self.basis.space

    @property
    def column_space(self):
        return self.basis.space

    @property
    def symmetry(self):
        return self.sym

    def symmetrized(self, sym):
        if not isinstance(self.sym, TrivialGroup):
            raise TypeError('Only operators with trivial symmetry group '
                            'may be symmetrized.')
        return Projector(self.basis, sym)


class HalfspaceProjector(Operator):
    """A projector that is symmetrized and then truncated to some half-space.

    A fully symmetrized operator 'h_p' is written:

        h_p = ∑ g⁻¹ h_f g,

    where the sum runs over all elements of a given symmetry group.
    If we restrict to translation groups 'T', then we may define a
    subset 'X' of translations by specifying constraints on the
    translation group elements:

        ∑ a_j t_j ≤ c

    where a translation group element is written '(t_0, t_1, ..., t_n)'.
    and the 'a_j' and 'c' specify the constraint.

    Attributes
    ----------
    sym : TranslationGroup
    constraint : tuple of int
        Has length 'len(sym.generators) + 1'. All the elements except the
        last one specify the 'a_j', and the final element specifies the
        'c', using the notation above.
    """

    def __init__(self, basis, sym, constraint):
        if not isinstance(sym, TranslationGroup):
            raise TypeError('Halfspace projectors may only be defined for '
                            'translational symmetry groups.')
        super().__init__(basis, sym)
        self.operator = operator
        # XXX: Check for consistency of constraint
        constraint = tuple(constraint)
        if len(constraint) != len(sym.generators) + 1:
            raise ValueError('Constraint has wrong size.')
        self.constraint = constraint

    @property
    def row_space(self):
        return self.operator.row_space

    @property
    def column_space(self):
        return self.operator.column_space

    @property
    def symmetry(self):
        return TrivialGroup()


def project(operator, projector, include_hoppings=False):
    return Projected(operator, projector, include_hoppings)


class Projected(Operator):
    """A product of an operator and a diagonal projector.

    Consider an operator 'A' and a diagonal projector 'P'. An object
    of type 'Projected' represents either the expression:

        PAP,

    if 'include_hoppings == False', or

        PA + AP + PAP

    if 'include_hoppings == True'.

    Attributes
    ----------
    operator : Operator
    projector : Projector
    include_hoppings : bool
    """
    def __init__(self, operator, projector, include_hoppings):
        if not (operator.row_space.has_subspace(projector.row_space) and
                operator.column_space.has_subspace(projector.column_space)):
            raise ValueError('Hilbert spaces of projector and operator '
                             'are incompatible.')
        if not operator.symmetry.has_subgroup(projector.symmetry):
            raise ValueError('Projector must have a lower symmetry than the '
                             'operator being projected.')
        self.projector = projector
        self.operator = operator
        self.include_hoppings = include_hoppings

    @property
    def row_space(self):
        return self.operator.space

    @property
    def column_space(self):
        return self.operator.space

    @property
    def symmetry(self):
        return self.projector.symmetry


# High-level tools

def attach_lead(H_lead, H_syst):
    """Attach a 1D lead to a 0D scattering region.

    Parameters
    ----------
    H_lead : Operator
        Symmetry must be 1D translation only.
    H_syst : Explicit or Projected
        The scattering region. Must have 0D translation only

    Returns
    -------
    H_lead : HalfSpace
        The lead truncated at the system boundary.
    H_syst : Algebra
        A sum of the original scattering region and
        potentially a projected region of the lead.
    """
